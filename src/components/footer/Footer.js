import React from 'react';
import './Footer.css';

const Footer = () => {
    const logo = `/assets/images/logo.png`;
    const logo2 = `/assets/images/logo2.png`;
    
    return (
        <>
            <div className='footerContainer'>
                <div className='imageWrapperFooterOne'>
                    <img className="logoFooter" alt="logo" src={logo} />
                </div>
                <div className='linksFooter'>
                    <ul className='linksFooterList'>
                        <li>
                            <a href="#">
                                <span>inicio</span>
                            </a>
                        </li>
                        <li> 
                            <a href="#">
                                <span>nosotros</span>  
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>restaurante</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>reservaciones</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>catering</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>food trucks</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>jh foods</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>contacto</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className='contactFooter'>
                    <ul className='linksFooterList'>
                        <li>
                            <span>Contáctanos</span>
                        </li>
                        <li> 
                            <a href="#">
                                <span> (786)277-7807</span>  
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span> jhfoodllanos@gmail.com</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div className='imageWrapperFooterTwo'>
                    <img className="logoFooter2" alt="logo2" src={logo2} />
                </div>

            </div>
            <div className='endFooter'>
                <div className='privacyContainer'>
                    <div className='privacy'>
                        <a href="#">
                            <span>Política de privacidad</span>
                        </a>
                        <div className='separator'></div>
                        <a href="#">
                            <span>Política de reembolso</span>
                        </a>
                        <div className='separator'></div>
                        <a href="#">
                            <span>Términos y condiciones</span>
                        </a>
                    </div>
                    <div className='madeBy'>
                        <span>© 2022 JH. Todos los derechos reservados. Creado por estigon</span>
                    </div>

                </div>
                <div className='socialNetworks'>
                    <a href="#">
                        <span><i className="fa-brands fa-twitter"></i></span>
                    </a>
                    <a href="#">
                        <span><i className="fa-brands fa-facebook"></i></span>
                    </a>
                    <a href="#">
                        <span><i className="fa-brands fa-instagram"></i></span>
                    </a>
                    <a href="#">
                        <span><i className="fa-brands fa-youtube"></i></span>
                    </a>
                </div>

            </div>
        </>
    )
}

export default Footer