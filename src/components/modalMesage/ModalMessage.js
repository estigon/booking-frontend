import React from 'react';
import './ModalMessage.css';

const ModalMessage = ({title, message, close}) => {
  return (
      <div className='modalMessageBackground'>
          <div className='modalMessageContainer'>
            <div className='titleCloseBtn'>
              <i className="fa-solid fa-xmark" onClick={close}></i>
            </div>
            <div className='modalMessageTitle'>
                <h6>{title}</h6>
            </div>
            <div className='modalMessageBody'>
                <span>{message}</span>
            </div>
            <div className="modalMessageFooter">
              <button id='accept' onClick={close}>Aceptar</button>
            </div>
          </div>

      </div>
  )
}

export default ModalMessage