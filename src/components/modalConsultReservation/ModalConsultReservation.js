import React from 'react';
import {RotatingLines} from 'react-loader-spinner';
import './ModalConsultReservation.css';

const ModalConsultReservation = ({isConsult}) => {
  return (
      <div className='modalBackground'>
          <div className='modalContainer'>
            <div className='modalTitle'>
                { isConsult ? <h6>Consultando disponibilidad</h6> : <h6>Guardando reservación</h6> }
            </div>
            <div className='modalBody'>
                <div className='loaderContainer'>
                    <RotatingLines width="100" strokeColor="#fe0000"  />
                </div>
            </div>
          </div>

      </div>
  )
}

export default ModalConsultReservation