import React, { useState } from 'react';
import './Navbar.css';

const Navbar = () => {
    const logo = `/assets/images/logo.png`;
    const [show, setShow] = useState(false);

    return (
        <div>
            <div className='navbar-wrapper'>
                <nav className="nav-bar">
                    <img className="logo" alt="logo" src={logo} />
                    <ul className={`menu-items ${ show ? 'show' : ''}`}>
                        <li>
                            <a href="#">
                                <span>inicio</span>
                            </a>
                        </li>
                        <li> 
                            <a href="#">
                                <span>nosotros</span>  
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>restaurante</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span className='link-active'>reservaciones</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>catering</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>food trucks</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>jh foods</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>contacto</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span>
                                    <i className="fa-brands fa-whatsapp"></i>
                                    &nbsp;
                                    whatsapp
                                </span>
                            </a>
                        </li>
                    </ul>
                    <span 
                        className="hamburger-icon"
                        onClick={() => setShow( beforeValue => !beforeValue)} 
                    >
                    { (!show) ? <i className="fa-solid fa-bars"></i> : <i className="fa-solid fa-xmark"></i> }
                    </span>
                </nav>
            </div>
            <div className='title-wrapper'>
                <div>
                    <h1>Reservaciones</h1>
                </div>
            </div>
        </div>
    )
}

export default Navbar