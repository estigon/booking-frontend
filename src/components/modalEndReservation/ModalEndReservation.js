import React from 'react';
import './ModalEndReservation.css';

const ModalEndReservation = ({ close, telephone, setTelephone, name, setName, saveReservation }) => {
  return (
      <div className='modalEndReservationBackground'>
          <div className='modalEndReservationContainer'>
            <div className='titleCloseBtn'>
              <i className="fa-solid fa-xmark" onClick={close}></i>
            </div>
            <div className='modalEndReservationTitle'>
                <h6>Crear Reserva</h6>
            </div>
            <div className='modalEndReservationBody'>
              <form id='endModalForm' onSubmit={saveReservation}>
                <div className='fieldEndModalEndReservation'>
                  <label>Nombre</label>
                  <input
                      value={name}
                      onChange = {setName}
                      type='text'
                      name='description'
                      placeholder='Introduce tu nombre'
                      autoComplete='off'
                  />
                </div>
                <div className='fieldEndModalEndReservation'>
                  <label>Telefono</label>
                  <input
                      value={telephone}
                      onChange = {setTelephone}
                      type='text'
                      name='description'
                      placeholder='Introduce tu número de telf.'
                      autoComplete='off'
                  />
                </div>
              </form>
            </div>
            <div className="modalEndReservationFooter">
              <button id='accept' type='submit' onClick={saveReservation}>Guardar</button>
            </div>
          </div>

      </div>
  )
}

export default ModalEndReservation;