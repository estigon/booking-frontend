import React, { useState } from 'react';
import './FormReservation.css';
import { HOURS } from '../../utils/hours';
import ModalConsultReservation from '../modalConsultReservation/ModalConsultReservation';
import ModalMessage from '../modalMesage/ModalMessage';
import ModalEndReservation from '../modalEndReservation/ModalEndReservation';
import { postRequest, baseURL } from './../../utils/axiosClient';

const FormReservation = () => {

    const [numberPerson, setNumberPerson] = useState(0);
    const [date, setDate] = useState(new Date().toISOString().slice(0,10));
    const [hour, setHour] = useState(8);
    const [showModalConsultReservation, setShowModalConsultReservation] = useState(false);
    const [showModalMessage, setShowModalMessage] = useState(false);
    const [modalMessageTitle, setModalMessageTitle] = useState('');
    const [modalMessageText, setModalMessageText] = useState('');

    const [showModalEndReservation, setShowModalEndReservation] = useState(false);
    const [name, setName] = useState('');
    const [telephone, setTelephone] = useState('');

    const [isConsult, setIsConsult] = useState(false);

    
    const handleInputChange = (e) => {
        e.preventDefault();

        let number = '';
        if(e.target.value) {
           number = parseInt(e.target.value);
        }

        setNumberPerson(number);
    }

    const handleDateChange = (e) => {
        e.preventDefault();
        const date = e.target.value;
        setDate(date);
    }

    const handleHourChange = (e) => {
        e.preventDefault();
        const hour = parseInt(e.target.value);
        setHour(hour);
    }

    const handleNameChange = (e) => {
        e.preventDefault();
        const name = e.target.value;
        setName(name);
    }

    const handleTelephoneChange = (e) => {
        e.preventDefault();
        const telephone = e.target.value;
        setTelephone(telephone);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        if( !(/^\d*$/.test(numberPerson)) || numberPerson === '' || numberPerson <= 0 ){
            setShowModalMessage(true);
            setModalMessageTitle('Información');
            setModalMessageText('Introduce un número válido de personas.');
            return;
        }

        setIsConsult(true);
        handleModalConsultReservationOpen();

        try {
            // make request
            const response = await postRequest(`${baseURL}/api/bookings/availables`, {numberPerson, date, hour});
            const { isAvailable, message } = response.data;

            handleModalConsultReservationClose();
            setShowModalMessage(true);
            setModalMessageTitle('Información');
            setModalMessageText(message);

            if(isAvailable){
                setShowModalEndReservation(true);
            }

        } catch (error) {
            handleModalConsultReservationClose();
            setShowModalMessage(true);
            setModalMessageTitle('Error');
            setModalMessageText('Ocurrio un error. Por favor, consultar con el administrador del sitio.');
        }
    }

    const handleModalConsultReservationOpen = () => {
        setShowModalConsultReservation(true);
    }

    const handleModalConsultReservationClose = () => {
        setIsConsult(false);
        setShowModalConsultReservation(false);
    }

    const handleModalMessageClose = () => {
        setModalMessageTitle('');
        setModalMessageText('')
        setShowModalMessage(false);
    }

    const handleModalEndReservationClose = () => {
        setName('');
        setTelephone('');
        setShowModalEndReservation(false);
    }

    const resetForms = () => {
        setDate(new Date().toISOString().slice(0,10));
        setHour(8);
        setNumberPerson(0);
    }

    const saveReservation = async (e) => {
        e.preventDefault();

        if(name === ''){
            setShowModalMessage(true);
            setModalMessageTitle('Información');
            setModalMessageText('Introduce un nombre para continuar');
            return;
        }

        if(telephone === ''){
            setShowModalMessage(true);
            setModalMessageTitle('Información');
            setModalMessageText('Introduce un número de telefono para continuar');
            return;
        }

        try {
            
            // hide reservation modal
            setShowModalEndReservation(false);

            // show loading modal
            handleModalConsultReservationOpen();

            await new Promise((resolve, rej) => {
                setTimeout(() => {
                    resolve('listo')
                }, 3000);
            })

            // request
            const response = await postRequest(`${baseURL}/api/bookings`, {numberPerson, date, hour, name, telephone});

            const { status } = response;

            // after the response hide loading modal
            handleModalConsultReservationClose();
            
            // clear the form
            handleModalEndReservationClose();
            resetForms();
            

            if( status === 201 ) {
            // show message modal
                setShowModalMessage(true);
                setModalMessageTitle('Información');
                setModalMessageText('Reservación creada exitosamente.');
            }
        } catch (error) {
            let message = error?.response?.data?.message || 'Ocurrio un error. Por favor, consultar con el administrador del sitio.';

            if(message instanceof Array) {
                message = message[0]
            }

            // after the response hide loading modal
            handleModalConsultReservationClose();

            // clear end form
            handleModalEndReservationClose();

            // show message modal
            setShowModalMessage(true);
            setModalMessageTitle('Información');
            setModalMessageText(message);
        }
    }

    return (
        <>
            <div className='container'>
                <div className='text-wrapper'>

                    <h2>Visita JH Carne en Vara a la Llanera</h2>
                    <h3>
                        Si deseas vivir la experiencia llanera de nuestro restaurante en Kendall, solo debes rellenar tus datos y realizar una reservación totalmente gratuita.
                    </h3>
                </div>
                <form onSubmit={ handleSubmit }>
                    <div className='field-wrapper'>

                        <label>Cantidad de personas</label>
                        <input
                            value={numberPerson}
                            onChange = {handleInputChange}
                            type='text'
                            name='description'
                            placeholder='Introduce el número de personas'
                            autoComplete='off'
                        />
                    </div>
                    <div className='field-wrapper'>
                        <label>Fecha</label>
                        <input
                            value={date}
                            onChange = {handleDateChange}
                            type='date'
                            name='description'
                            autoComplete='off'
                        />
                    </div>
                    <div className='field-wrapper'>
                        <label>Hora</label>
                        <select value={hour} onChange={handleHourChange}>
                            {
                                HOURS.map(hour => (
                                    <option key={hour.id} value={hour.value}>{hour.text}</option>
                                ))
                            }
                        </select>
                    </div>
                    <div className='button-wrapper'>
                        <button id="continue" type='submit'>Continuar</button>
                    </div>
                </form>
            </div>
            { showModalConsultReservation && <ModalConsultReservation isConsult={isConsult}/>}
            { showModalMessage && <ModalMessage close={handleModalMessageClose} title={modalMessageTitle} message={modalMessageText} /> }
            { showModalEndReservation && 
                <ModalEndReservation 
                    close={handleModalEndReservationClose} 
                    name={name}
                    setName={handleNameChange}
                    telephone={telephone}
                    setTelephone={handleTelephoneChange}
                    saveReservation={saveReservation}
                /> 
            }
        </>
    )
}

export default FormReservation;