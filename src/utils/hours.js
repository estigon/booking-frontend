export const HOURS = [
    {
        id: 8,
        value: 8,
        text: '8:00 am'
    },
    {
        id: 9,
        value: 9,
        text: '9:00 am'
    },
    {
        id: 10,
        value: 10,
        text: '10:00 am'
    },
    {
        id: 11,
        value: 11,
        text: '11:00 am'
    },
    {
        id: 12,
        value: 12,
        text: '12:00 pm'
    },
    {
        id: 13,
        value: 13,
        text: '1:00 pm'
    },
    {
        id: 14,
        value: 14,
        text: '2:00 pm'
    },
    {
        id: 15,
        value: 15,
        text: '3:00 pm'
    },
    {
        id: 16,
        value: 16,
        text: '4:00 pm'
    },
    {
        id: 17,
        value: 17,
        text: '5:00 pm'
    },
    {
        id: 18,
        value: 18,
        text: '6:00 pm'
    },
]