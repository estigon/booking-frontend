import React from 'react';
import Footer from '../components/footer/Footer';
import FormReservation from '../components/formReservation/FormReservation';
import Navbar from '../components/navbar/Navbar';

const ReservationScreen = () => {
  return (
      <>
        <Navbar />
        <FormReservation />
        <Footer />
      </>
  )
}

export default ReservationScreen;